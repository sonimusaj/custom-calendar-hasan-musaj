package hasan.musaj.customcalendar.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import hasan.musaj.customcalendar.R;

public class DateViewHolder extends RecyclerView.ViewHolder {

    public TextView dateText;
    public RelativeLayout container;

    public DateViewHolder(View itemView) {
        super(itemView);
        dateText = itemView.findViewById(R.id.date_text);
        container = itemView.findViewById(R.id.date_container);
    }
}