package hasan.musaj.customcalendar.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import hasan.musaj.customcalendar.R;

public class DayViewHolder extends RecyclerView.ViewHolder {

    public TextView dayText;

    public DayViewHolder(View itemView) {
        super(itemView);

        dayText = itemView.findViewById(R.id.day_text);
    }
}