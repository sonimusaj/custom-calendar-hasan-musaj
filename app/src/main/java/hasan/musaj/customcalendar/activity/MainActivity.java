package hasan.musaj.customcalendar.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import hasan.musaj.customcalendar.R;
import hasan.musaj.customcalendar.adapter.CalendarViewPagerAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager calendarPager = findViewById(R.id.main_viewpager);
        CalendarViewPagerAdapter adapter = new CalendarViewPagerAdapter(getSupportFragmentManager());
        calendarPager.setAdapter(adapter);
        calendarPager.setOffscreenPageLimit(6);
    }
}
