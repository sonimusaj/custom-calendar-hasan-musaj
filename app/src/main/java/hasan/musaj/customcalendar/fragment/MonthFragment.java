package hasan.musaj.customcalendar.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;

import hasan.musaj.customcalendar.R;
import hasan.musaj.customcalendar.adapter.MonthRecyclerViewAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class MonthFragment extends Fragment {

    public static final String VIEWPAGER_POSITION = "position";
    private Calendar selectedMonth;

    public static MonthFragment getInstance(int position){
        Bundle arguments = new Bundle();
        arguments.putInt(VIEWPAGER_POSITION, position);
        MonthFragment fragment = new MonthFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public MonthFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int position = getArguments().getInt(VIEWPAGER_POSITION);

        selectedMonth = Calendar.getInstance();
        selectedMonth.set(Calendar.DATE, 1);
        //selected month is equal to current month(position 0 of viewpager) + the current position
        selectedMonth.add(Calendar.MONTH, position);

        return inflater.inflate(R.layout.fragment_month, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView monthNameTextView = view.findViewById(R.id.month_name);
        RecyclerView monthRecycler = view.findViewById(R.id.month_recycler);
        MonthRecyclerViewAdapter adapter = new MonthRecyclerViewAdapter(getActivity(), selectedMonth);

        monthRecycler.setAdapter(adapter);

        String[] months = getResources().getStringArray(R.array.month_names);
        monthNameTextView.setText(months[selectedMonth.get(Calendar.MONTH)] + " / " + selectedMonth.get(Calendar.YEAR));

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 7){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        monthRecycler.setLayoutManager(layoutManager);
    }
}
