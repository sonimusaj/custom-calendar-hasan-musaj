package hasan.musaj.customcalendar.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import hasan.musaj.customcalendar.fragment.MonthFragment;

public class CalendarViewPagerAdapter extends FragmentStatePagerAdapter {

    public CalendarViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return MonthFragment.getInstance(position);
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

}
