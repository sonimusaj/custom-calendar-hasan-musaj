package hasan.musaj.customcalendar.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;

import hasan.musaj.customcalendar.R;
import hasan.musaj.customcalendar.ViewHolder.DateViewHolder;
import hasan.musaj.customcalendar.ViewHolder.DayViewHolder;

public class MonthRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int DAY_HEADER_TYPE = 0;
    private static final int DATE_TYPE = 1;
    private int datesOffset = 0;
    private String[] dayNames;
    private Context context;
    private Calendar selectedMonth;
    private Calendar currentDate;

    public MonthRecyclerViewAdapter(Context context, Calendar selectedMonth){
        this.context = context;
        this.selectedMonth = selectedMonth;
        dayNames = context.getResources().getStringArray(R.array.week_days);
        currentDate = Calendar.getInstance();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case DAY_HEADER_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_item, parent, false);
                return new DayViewHolder(view);
            case DATE_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.date_item, parent, false);
                return new DateViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_item, parent, false);
                return new DayViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case DAY_HEADER_TYPE:
                ((DayViewHolder)holder).dayText.setText(dayNames[position]);
                break;
            case DATE_TYPE:
                //first 7 positions  are reserved from the header -1 position to start from date 1
                int datePosition = position - 6;

                selectedMonth.set(Calendar.DATE, 1);
                int firstDayOfMonth = selectedMonth.get(Calendar.DAY_OF_WEEK) - 1;

                //sunday is first day of calendar so send it at the last position
                if(firstDayOfMonth == 0){
                    firstDayOfMonth = 7;
                }

                if(datePosition < firstDayOfMonth){
                    //month has not started yet
                    datesOffset++;
                }else if(datePosition - datesOffset > selectedMonth.getActualMaximum(Calendar.DATE)){
                    //month has finished
                }else{
                    //positioned date is holder position - positions before month's start
                    int dateNumber = datePosition - datesOffset;

                    selectedMonth.set(Calendar.DATE, dateNumber);

                    //change weekend days color
                    if(selectedMonth.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || selectedMonth.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                        ((DateViewHolder) holder).dateText.setTextColor(context.getResources().getColor(R.color.weekend_color));
                    }

                    ((DateViewHolder) holder).dateText.setText("" + dateNumber);

                    //change color of current day
                    if(currentDate.get(Calendar.DATE) == selectedMonth.get(Calendar.DATE) &&
                            currentDate.get(Calendar.MONTH) == selectedMonth.get(Calendar.MONTH) &&
                            currentDate.get(Calendar.YEAR) == selectedMonth.get(Calendar.YEAR)){
                        ((DateViewHolder) holder).dateText.setTextColor(context.getResources().getColor(R.color.lightBlue));
                        ((DateViewHolder) holder).container.setBackground(context.getResources().getDrawable(R.drawable.selected_day_background));
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 49;
    }

    @Override
    public int getItemViewType(int position) {
        if(position < 7) return DAY_HEADER_TYPE;
        else return DATE_TYPE;
    }

}
